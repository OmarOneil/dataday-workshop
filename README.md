## Welcome to Dataday 2022 workshop

This workshop shows very basic example of [the data mesh](https://www.datamesh-architecture.com/) architecture use case.

Imagine you are an owner of data in your domain-specific database and you want to expose the data for the other teams in
your company, so they can combine them with other domain-specific dataset. You want to make sure the data are fresh and 
everyone in the company knows you are the owner. 

Right now, the industry-standard solution is using the data mesh architecture and the [Apache Airflow](https://airflow.apache.org/)
as a data flow orchestrator. 

For simplification of this workshop your domain-specific data are already in the [Google Cloud Storage (GCS)](https://cloud.google.com/storage). 
Your goal is to get the data into [Google BigQuery (BQ)](https://cloud.google.com/bigquery) by building data pipeline 
using a [DAG](https://airflow.apache.org/docs/apache-airflow/stable/concepts/dags.html). 

The data you are going to work with
are gathered during your customer's interactions with your web application. In the second part of this workshop you will
explore this data and use them to improve your app UX, evaluate A/B tests etc.

Take a look at the following diagram to understand the basic architecture you are going to use:
<div align="center">
  <img alt="active shell" src=".info/imgs/diagram.png" width="75%"/>
</div>


### Main task
  
*Load JSON data from Google bucket to Bigquery using Airflow (Google composer)*

### Workshop guide

Here are all necessary steps. Every step which requires some input contains also hidden correct answer. This answer is collapsible. Use only in case of emergency ;)

#### 1. Log in to our Google project

* Log in to your Google account you shared with us.
* Go to the project [homepage](https://console.cloud.google.com/home/dashboard?project=dataday-2022-375de387).

#### 2. Activate Cloud Shell and web editor
* Click on the `Activate Cloud Shell` in the right top corner and wait for the initialization.
  <img alt="active shell" src=".info/imgs/activate_shell.png" width="75%"/>

* You can now switch between the `editor` and `terminal` by the corresponding button.
  <img alt="open terminal" src=".info/imgs/open_terminal.png" width="75%"/>

#### 3. Create project structure
Create folders:
```
└── <your_name_workshop>/
    └──dags/
    └──schemas/
```


<details>
    <summary>Example:</summary>
    <img src=".info/imgs/folder_structure.png" width="40%"/>

</details>

#### 4. Design Bigquery table schemas
Create BQ JSON schemas following the [Google docs](https://cloud.google.com/bigquery/docs/schemas#specifying_a_json_schema_file)
and the details bellow and insert them into schemas folder:

**Frontend events table:**
```
NAME        DATA TYPE
timestamp   date
visitorId   string
platform    string
browser     string
os          string
language    string
module      string
category    string
action      string
test_name   string
test_group  string
```

<details>
    <summary>Correct solution:</summary>
    Here is frontend schema json:

```json
[
    {
      "name": "timestamp",
      "type": "DATE"
    },
    {
      "name": "visitorId",
      "type": "STRING"
    },
    {
      "name": "platform",
      "type": "STRING"
    },
    {
      "name": "browser",
      "type": "STRING"
    },
    {
      "name": "os",
      "type": "STRING"
    },
    {
      "name": "language",
      "type": "STRING"
    },
    {
      "name": "module",
      "type": "STRING"
    },
    {
      "name": "category",
      "type": "STRING"
    },
    {
      "name": "rep",
      "type": "STRING"
    },
    {
      "name": "action",
      "type": "STRING"
    },
    {
      "name": "test_name",
      "type": "STRING"
    },
    {
      "name": "test_group",
      "type": "STRING"
    }
  ]
```
</details>

**Booking table:**
```
NAME                    DATA TYPE
timestamp               date
bid                     integer
expected_net_revenue    float
```
<details>
    <summary>Correct solution:</summary>
    Here is booking schema json:

```json
[
    {
      "name": "timestamp",
      "type": "DATE"
    },
    {
      "name": "bid",
      "type": "INTEGER"
    },
    {
      "name": "expected_net_revenue",
      "type": "FLOAT64"
    }
  ]
```
</details>


#### 5. Write your terraform file
* In the editor create `main.tf` file from the [main.tf.template](./main.tf.template) next to the `<your_name_workshop>` folder.
* Fill the missing values for `google_bigquery_dataset` and `google_bigquery_table` resources.
* The project id is `dataday-2022-375de387`.

<details>
    <summary>Correct solution (example):</summary>
    Terraform file example:

```terraform
    provider "google" {
      project = "dataday-2022-375de387"
      region  = "europe-west1"
    }
    
    resource "google_bigquery_dataset" "maximilian_yakovski_dataset" {
      dataset_id  = "maximilian_yakovski_dataset"
      location    = "EU"
      description = "my_dataset"
    }
    
    resource "google_bigquery_table" "maximilian_yakovski_dataday_table_booking" {
      dataset_id  = google_bigquery_dataset.maximilian_yakovski_dataset.dataset_id
      table_id    = "booking"
      description = "Booking records"
      time_partitioning {
        type                     = "DAY"
        field                    = "timestamp"
        require_partition_filter = "false"
      }
      schema = file("maximilian_yakovski_workshop/schemas/booking.json")
    }
    resource "google_bigquery_table" "maximilian_yakovski_dataday_table_frontend" {
      dataset_id  = google_bigquery_dataset.maximilian_yakovski_dataset.dataset_id
      table_id    = "frontend"
      description = "Frontend records"
      time_partitioning {
        type                     = "DAY"
        field                    = "timestamp"
        require_partition_filter = "false"
      }
      schema = file("maximilian_yakovski_workshop/schemas/frontend.json")
    }
```
  Location in project workspace:

  <img src=".info/imgs/terraform_location.png" width="50%"/>

</details>

#### 6. Build your Bigquery infrastructure 
* Go to the Cloud terminal, **set terminal to root directory** and run init command to initialize terraform state:
```bash
terraform init
```
* After you fill the values you should plan the changes and verify the command output:
```bash
terraform plan
```
* If the plan contains expected values you can apply the changes by the following command (write 'yes' when prompt appears):
```bash
terraform apply
```

By that you should have the BQ objects ready to be used by the DAG you are gonna create now.

#### 7. Create your own DAG file

Add new python file to your folder structure:
```
└── <your_name_workshop>/
    └──dags/
        └── <your-unique-dag-name>.py
```

Take a look at the following code you can get inspired by. The DAG reads data from GCS and
stores them to a BQ table. You need to fill the missing values and add additional Airflow task for copying data to the
second table.

```python
import json
from datetime import timedelta, datetime
from pathlib import Path

from airflow import DAG
from airflow.providers.google.cloud.hooks.bigquery import BigQueryHook
from airflow.providers.google.cloud.sensors.gcs import GCSObjectExistenceSensor
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator
from airflow.operators.python import PythonOperator
from google.cloud.bigquery import SourceFormat, WriteDisposition

DATA_BUCKET = "dataday_data"
EVENTS_FILE_NAME = "data/events_{{ ts_nodash }}.jsonl"
BOOKINGS_FILE_NAME = "data/bookings_{{ ts_nodash }}.jsonl"
CONN_ID = "gcs_to_bq"
TABLE = "dataday-2022-375de387.<dataset>.<table>"

def print_bq_metadata():
    hook = BigQueryHook(
        gcp_conn_id=CONN_ID,
    )
    tabledata = hook.get_client().get_table(TABLE)
    print(f"# of rows: {tabledata.num_rows}")


with DAG(
        dag_id="<your-unique-dag-name>",
        schedule_interval=timedelta(minutes=10),
        start_date=datetime(2022, 3, 30, 0, 0, 0),
        max_active_runs=1,
        catchup=True,
) as dag:
    events_file_exists = GCSObjectExistenceSensor(
        google_cloud_conn_id=CONN_ID,
        bucket=DATA_BUCKET,
        object=EVENTS_FILE_NAME,
        task_id="gcs_file_exists",
        timeout=60,
    )

    print_bq_metadata = PythonOperator(
        task_id='print_bq_metadata',
        python_callable= print_bq_metadata,
    )
    
    """
    The GCSToBigQueryOperator allows loading BQ schema from GCS (Google Cloud Storage) but we don't use it right now.
    Therefore, we need to load the schema file from the disk and use it as the `schema_fields` parameter. 
    """
    event_schema_text = (Path(__file__).resolve().parent / "../schemas/<your-schema-file>.json").read_text()
    event_schema_json = json.loads(event_schema_text)

    """
    This operator will load JSON files stored in GCS and insert them into BQ.
    """
    events_gcs_to_bq = GCSToBigQueryOperator(
        task_id="gcs_to_bq",
        bucket=DATA_BUCKET,
        source_objects=EVENTS_FILE_NAME,
        schema_fields=event_schema_json,
        autodetect=False,
        destination_project_dataset_table=TABLE,
        source_format=SourceFormat.NEWLINE_DELIMITED_JSON,
        write_disposition=WriteDisposition.WRITE_APPEND,
        gcp_conn_id=CONN_ID,
    )

    """
    This code determines the tasks workflow. The tasks in [...] brackets run in parallel.
    The arrow symbols >> configure sequential dependency.
    """
    events_file_exists >> [print_bq_metadata, events_gcs_to_bq]

    """
    Now, you should add the code for the second BQ table - bookings.
    
    Make sure the `task_id` values are unique in the whole DAG.
    """
    # bookings_file_exists = GCSObjectExistenceSensor(...)
    # bookings_gcs_to_bq = GCSToBigQueryOperator(...)
    # bookings_file_exists >> bookings_gcs_to_bq
```


<details>
    <summary>Correct solution (Example):</summary>
    Example of DAG implementation:

```python
import json
from datetime import timedelta, datetime
from pathlib import Path

from airflow import DAG
from airflow.providers.google.cloud.hooks.bigquery import BigQueryHook
from airflow.providers.google.cloud.sensors.gcs import GCSObjectExistenceSensor
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator
from airflow.operators.python import PythonOperator
from google.cloud.bigquery import SourceFormat, WriteDisposition

DATA_BUCKET = "dataday_data"
EVENTS_FILE_NAME = "data/events_{{ ts_nodash }}.jsonl"
BOOKINGS_FILE_NAME = "data/bookings_{{ ts_nodash }}.jsonl"
CONN_ID = "gcs_to_bq"
TABLE_FRONTEND = "dataday-2022-375de387.maximilian_yakovski_dataset.frontend"
TABLE_BOOKING = "dataday-2022-375de387.maximilian_yakovski_dataset.booking"

def print_bq_metadata(table):
    hook = BigQueryHook(
        gcp_conn_id=CONN_ID,
    )
    tabledata = hook.get_client().get_table(table)
    print(f"# of rows: {tabledata.num_rows}")


with DAG(
        dag_id="maximilian_yakovski_dag",
        schedule_interval=timedelta(minutes=10),
        start_date=datetime(2022, 3, 30, 0, 0, 0),
        max_active_runs=1,
        catchup=True,
) as dag:
    events_file_exists = GCSObjectExistenceSensor(
        google_cloud_conn_id=CONN_ID,
        bucket=DATA_BUCKET,
        object=EVENTS_FILE_NAME,
        task_id="events_gcs_file_exists",
        timeout=60,
    )

    events_print_bq_metadata = PythonOperator(
        task_id='events_print_bq_metadata',
        python_callable= print_bq_metadata,
        op_kwargs={'table': TABLE_FRONTEND}
    )

    event_schema_text = (Path(__file__).resolve().parent / "../schemas/frontend.json").read_text()
    event_schema_json = json.loads(event_schema_text)

    events_gcs_to_bq = GCSToBigQueryOperator(
        task_id="events_gcs_to_bq",
        bucket=DATA_BUCKET,
        source_objects=EVENTS_FILE_NAME,
        schema_fields=event_schema_json,
        autodetect=False,
        destination_project_dataset_table=TABLE_FRONTEND,
        source_format=SourceFormat.NEWLINE_DELIMITED_JSON,
        write_disposition=WriteDisposition.WRITE_APPEND,
        gcp_conn_id=CONN_ID,
    )

    booking_file_exists = GCSObjectExistenceSensor(
        google_cloud_conn_id=CONN_ID,
        bucket=DATA_BUCKET,
        object=BOOKINGS_FILE_NAME,
        task_id="bookings_gcs_file_exists",
        timeout=60,
    )

    booking_print_bq_metadata = PythonOperator(
        task_id='bookings_print_bq_metadata',
        python_callable= print_bq_metadata,
        op_kwargs={'table': TABLE_BOOKING}
    )

    booking_schema_text = (Path(__file__).resolve().parent / "../schemas/booking.json").read_text()
    booking_schema_json = json.loads(booking_schema_text)

    booking_gcs_to_bq = GCSToBigQueryOperator(
        task_id="bookings_gcs_to_bq",
        bucket=DATA_BUCKET,
        source_objects=BOOKINGS_FILE_NAME,
        schema_fields=booking_schema_json,
        autodetect=False,
        destination_project_dataset_table=TABLE_BOOKING,
        source_format=SourceFormat.NEWLINE_DELIMITED_JSON,
        write_disposition=WriteDisposition.WRITE_APPEND,
        gcp_conn_id=CONN_ID,
    )

    events_file_exists >> [events_print_bq_metadata, events_gcs_to_bq]
    booking_file_exists >> [booking_print_bq_metadata, booking_gcs_to_bq]
```
</details>

#### 8. Upload your DAG file to GCS 
Once your DAG is ready you need to go back to the terminal and import the `<your_name_workshop>` folder containing all the related
files to Airflow using the following command:
```bash
gcloud composer environments storage dags import --project dataday-2022-375de387 --environment dataday-airflow --location europe-west1 --source <your_name_workshop>
```

Every-time you make any change to the DAG you need to run the command from above again. Please note that you need to wait
up to 30 seconds to see the DAG changes in Aiflow UI. 


#### 9. Check your very first DAG runs

Go to the [Airflow UI](https://76833f178dda454ebb5cc0ba4caf2951-dot-europe-west1.composer.googleusercontent.com/) and 
find there your DAG.


<details>
    <summary>Navigation:</summary>
  
Composer navigation in menu:  <br>
<img src=".info/imgs/composer_click.png" width="70%"/> 

Clicking on the environment: <br> 
<img src=".info/imgs/composer_env_click.png" width="70%"/>

Opening airflow UI:  <br>
<img src=".info/imgs/composer_airflow_ui.png" width="70%"/>

You can find all important information regarding the UI in [Airflow docs](https://airflow.apache.org/docs/apache-airflow/stable/ui.html#).
If your task fails you can check its logs by clicking on the `View Log` button in the [context menu](https://airflow.apache.org/docs/apache-airflow/stable/ui.html#task-instance-context-menu).
![](.info/imgs/logs_view.png)
</details>

#### 10. Check the data in BQ
Once the DAG is running and already has some green rectangles showing the successful run you can check the data in BQ.
<br>
<img src=".info/imgs/green_dagruns.png" width="40%"/>

To do that you need to go to the [BQ UI](https://console.cloud.google.com/bigquery?project=dataday-2022-375de387), expand
the `dataday-2022-375de387` node and pick your dataset. After expanding the dataset node you will see your tables. Pick some, 
check its details, preview and validate it contains the data.

#### 11. You did it! Congrats...
